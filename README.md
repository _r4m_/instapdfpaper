### Introduction

InstaPDFpaper is a free tool that let you choose which Instapaper folders you'd like to download.

### Deploy

To run it you need to deploy it on Heroku, adding as add-on Postgres, Redis and the Heroku Scheduler.

Then you have to configure the following config vars:

- `INSTAPAPER_KEY`
- `INSTAPAPER_SECRET`
- `MAILJET_API_KEY`
- `MAILJET_SECRET_KEY`
- `DROPBOX_ACCESS_TOKEN`

## Resources

- [Instapaper library](https://github.com/stve/instapaper)
- [Instapaper APIs](https://www.instapaper.com/api/full)