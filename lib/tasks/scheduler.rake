require 'dropbox'

desc "This task is called by the Heroku scheduler add-on"
task :expire_download_urls => :environment do
	puts "Removing expired downloadable PDFs zip files..."
	@dropbox_client ||= Dropbox::Client.new(ENV['DROPBOX_ACCESS_TOKEN'])
	DownloadableUrl.expired.find_each do |download|
		begin
			puts "Found a file at #{download.dropbox_url}"
			@dropbox_client.delete(download.dropbox_url)
			download.destroy
		rescue Dropbox::ApiError => e
			puts e.message
			if /(path_lookup\/not_found\/)/.match(e.message)
				download.destroy
			end
		end
	end
end