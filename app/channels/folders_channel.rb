class FoldersChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'folders'
  end
end