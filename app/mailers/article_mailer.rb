class ArticleMailer < ApplicationMailer

  default :from => "\"Robot\" <hello@filippozanella.com>"

  def send_zip(dropbox_zip_path, user_id)
    @user = User.find_by(id: user_id)
    @dropbox_url = dropbox_zip_path

    to = @user.email

    mail(:to => to, :subject => "INSTAPDFPAPER: the requested data have been exported!") do |format|
      format.html { render "mailer/article" }
    end
  end
end
