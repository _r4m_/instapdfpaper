class FoldersJob < ApplicationJob
  queue_as :default

  def perform(user_id)
    user = User.find_by(id: user_id)
    init_instapaper_client(user)

    @data = []

    limit = Rails.env.development? ? 1 : 500

    folder_bookmarks = @client.bookmarks({limit: limit}).bookmarks
    @data << {folder_id: 0, folder_title: "Root", bookmarks_count: folder_bookmarks.size}

    folders = @client.folders
    folders.each do |folder|
      folder_bookmarks = @client.bookmarks({limit: limit, folder_id: folder.folder_id}).bookmarks
      @data << {folder_id: folder.folder_id, folder_title: folder.display_title, bookmarks_count: folder_bookmarks.size}
    end
    # BEGIN - This is useful if we want to allow single article selection
    # folder_bookmarks = @client.bookmarks({limit: limit}).bookmarks
    # bookmarks_map = folder_bookmarks.map {|bookmark| {title: bookmark.title, url: bookmark.url, id: bookmark.bookmark_id} }
    # @data << {folder_id: 0, folder_title: "Root", bookmarks_count: folder_bookmarks.size, bookmarks: bookmarks_map}

    # folders = @client.folders
    # folders.each do |folder|
    #   folder_bookmarks = @client.bookmarks({limit: limit, folder_id: folder.folder_id}).bookmarks
    #   bookmarks_map = folder_bookmarks.map {|bookmark| {title: bookmark.title, url: bookmark.url, id: bookmark.bookmark_id} }
    #   @data << {folder_id: folder.folder_id, folder_title: folder.display_title, bookmarks_count: folder_bookmarks.size, bookmarks: bookmarks_map}
    # end
    # END

    ActionCable.server.broadcast('folders', {data: render_data(@data)})
  end

  private
    def render_data(data)
      InstapaperController.renderer.render(partial: 'instapaper/index_async', locals: { data: data })
    end

end
