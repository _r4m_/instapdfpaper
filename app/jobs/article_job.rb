require 'dropbox'
require 'zip'
require 'fileutils'

class ArticleJob < ApplicationJob
  queue_as :default

  def perform(folder_ids, user_id)
    user = User.find_by(id: user_id)
    init_instapaper_client(user)

    # 0) Select all bookmarks texts
    puts "Start selection of all bookmarks content"
    limit = 500
    all_bookmarks = []
    folder_ids.each do |id|
      puts "Select bookmarks of folder #{id}"
      if id == 0
        params = {limit: limit}
      else
        params = {limit: limit, folder_id: id}
      end
      folder_bookmarks = @client.bookmarks(params).bookmarks
      bookmarks_map = folder_bookmarks.map {|bookmark| {title: bookmark.title, url: bookmark.url, id: bookmark.bookmark_id} }
      all_bookmarks.concat(bookmarks_map)
    end

    # 1) Convert to PDF
    puts "Start to convert articles to PDFs"
    zip_filename = "#{user_id}_#{Time.now.strftime("%Y%jT%H%M%SZ")}_#{SecureRandom.hex(10)}"
    zip_dir = "tmp/"+zip_filename
    zip_filepath = zip_dir+".zip"
    FileUtils.mkdir_p(zip_dir)
    File.chmod(0777, zip_dir)

    begin
      all_bookmarks.each do |bookmark|
        puts "Convert bookmark #{bookmark[:id]}"
        filename = sanitize_filename("#{bookmark[:title]}")+".pdf"
        begin
          html = @client.get_text(bookmark[:id])
        rescue Instapaper::Error
          next
        end
        html = "<!DOCTYPE HTML><html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><title>#{bookmark[:title]}</title></head><body style='width: 100%;'>"+html+"</body></html>"
        renderer = ERB.new(html)
        b = binding
        b.local_variable_set(:bookmark, bookmark)
        output = renderer.result(b)
        html = "#{output}"
        kit = PDFKit.new(html, :page_size => 'Letter')
        pdf = kit.to_pdf
        kit.to_file(zip_dir+"/#{filename}")
      end

      zf = ZipFileGenerator.new(zip_dir, zip_filepath)
      zf.write()

      # 3) Save to Dropbox
      puts "Save ZIP file to Dropbox"
      @dropbox_client ||= Dropbox::Client.new(ENV['DROPBOX_ACCESS_TOKEN'])
      file = open(zip_filepath)
      if Rails.env.development?
        root_url = "/INSTAPAPER-TEST/"
      else
        root_url = "/INSTAPAPER/"
      end
      dropbox_url = root_url+zip_filename+".zip"
      dropbox_response = @dropbox_client.upload(dropbox_url, file)
      file.close
      # puts dropbox_response
      dropbox_temporary_link = @dropbox_client.get_temporary_link(dropbox_url)[1]
      DownloadableUrl.create(user: user, dropbox_url: dropbox_url)

      # 4) Send email with url
      puts "Send Dropbox temporary url via email"
      ArticleMailer.send_zip(dropbox_temporary_link, user_id).deliver_now!
    ensure
      if File.exist?(zip_filepath)
        puts "Save ZIP file to Dropbox"
        FileUtils.rm(zip_filepath)
        FileUtils.rm_rf(zip_dir)
      end
    end
  end

  private

    def sanitize_filename(filename)
      fn = filename.split(/(?<=.)\.(?=[^.])(?!.*\.[^.])/m)
      fn.map! { |s| s.gsub(/[^a-z0-9\-]+/i, '_') }
      return fn.join('.')
    end
end
