class ApplicationJob < ActiveJob::Base

	protected

	  def init_instapaper_client(user)
	    @client ||= Instapaper::Client.new do |client|
	      client.consumer_key = ENV['INSTAPAPER_KEY']
	      client.consumer_secret = ENV['INSTAPAPER_SECRET']
	      client.oauth_token = user.instapaper_token
	      client.oauth_token_secret = user.instapaper_token_secret
	    end
	  end

end
