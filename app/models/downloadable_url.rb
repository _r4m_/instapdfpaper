# == Schema Information
#
# Table name: downloadable_urls
#
#  id          :integer          not null, primary key
#  dropbox_url :string           not null
#  user_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class DownloadableUrl < ActiveRecord::Base
	belongs_to :user

	validates :dropbox_url, presence: true

	scope :expired, -> { where(["created_at < ?", 4.hours.ago]) }
end
