App.folders = App.cable.subscriptions.create('FoldersChannel', {
  received: function(data) {
    $('#cable-content').empty().append(data['data']);
    var table = document.querySelector('table');
    var boxes = table.querySelectorAll('tbody .mdl-data-table__select');
    for (var i = 0, length = boxes.length; i < length; i++) {
      mc = new MaterialCheckbox(boxes[i]);
      boxes[i].MaterialCheckbox = mc;
    };
  }
});