class InstapaperController < ApplicationController

  before_action :init_instapaper_client
  after_action :request_instapaper_folders, only: :index

  def index
    respond_to do |format|
      format.html
      format.js
    end
  end

  def export
    get_folder_ids

    @errors = {}
    @errors[:messages] = []

    if @folder_ids.nil? or @folder_ids.empty?
      @errors[:messages] << "You have to select at least one folder"
    else
      folders = @client.folders.map {|folder| {folder_id: folder.folder_id, folder_title: folder.display_title}}.push({folder_id: 0, folder_title: "Root"})
      @selected_folders = folders.select {|h| @folder_ids.include?(h[:folder_id])}
    end

    if @errors[:messages].empty?
      respond_to do |format|
        format.js
      end
    else
      respond_to do |format|
        format.js { render status: 422 }
      end
    end
  end

  def definitely_export
    get_folder_ids

    ArticleJob.perform_later(@folder_ids, current_user.id)
    respond_to do |format|
      format.js
    end
  end

private

  def init_instapaper_client
    @client ||= Instapaper::Client.new do |client|
      client.consumer_key = ENV['INSTAPAPER_KEY']
      client.consumer_secret = ENV['INSTAPAPER_SECRET']
      client.oauth_token = current_user.instapaper_token
      client.oauth_token_secret = current_user.instapaper_token_secret
    end
  end

  def get_folder_ids
    if params[:folder_ids].present?
      @folder_ids ||= params[:folder_ids].map! {|id| id.to_i}
    else
      @folder_ids = []
    end
  end

  def request_instapaper_folders
    FoldersJob.set(wait: 1.second).perform_later(current_user.id)
  end

end
