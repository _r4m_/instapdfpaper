class SessionsController < Devise::SessionsController

  def new
    super
  end

  def create

    build_resource(sign_in_params)

    client = Instapaper::Client.new do |client|
      client.consumer_key = ENV['INSTAPAPER_KEY']
      client.consumer_secret = ENV['INSTAPAPER_SECRET']
    end

    begin
      token = client.access_token(sign_in_params["email"], sign_in_params["password"])
      client.oauth_token = token.oauth_token
      client.oauth_token_secret = token.oauth_token_secret
      client.verify_credentials

      existing_resource = warden.authenticate(auth_options)

      if existing_resource.present?
        self.resource = existing_resource
        resource.instapaper_token = token.oauth_token
        resource.instapaper_token_secret = token.oauth_token_secret
        resource.save
        set_flash_message!(:notice, :signed_in)
        sign_in(resource_name, resource)
        yield resource if block_given?
        respond_with resource, location: after_sign_in_path_for(resource)
      else
        resource.instapaper_token = token.oauth_token
        resource.instapaper_token_secret = token.oauth_token_secret
        resource.save
        yield resource if block_given?
        if resource.persisted?
          set_flash_message!(:notice, :signed_in)
          sign_in(resource_name, resource)
          respond_with resource, location: after_sign_in_path_for(resource)
        else
          clean_up_passwords resource
          set_minimum_password_length
          respond_with resource
        end
      end
    rescue Instapaper::Error::ClientError
      respond_with resource, status: :unauthorized, location: after_sign_in_path_for(resource)
    end
  end

  private

    def build_resource(hash=nil)
      self.resource = resource_class.new(hash || {})
    end

end