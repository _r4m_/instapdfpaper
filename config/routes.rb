Rails.application.routes.draw do

  mount ActionCable.server => '/cable'

  devise_for :users, controllers: {sessions: "sessions"}

  root to: "instapaper#index"

  get "instapaper/export" => 'instapaper#export', as: "export"
  post "instapaper/definitely_export" => 'instapaper#definitely_export', as: "definitely_export"
end
