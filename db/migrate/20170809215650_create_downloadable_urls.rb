class CreateDownloadableUrls < ActiveRecord::Migration
  def change
    create_table :downloadable_urls do |t|
      t.string :dropbox_url, null: false

      t.belongs_to :user, index: true

      t.timestamps
    end
  end
end
